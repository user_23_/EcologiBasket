#include <Servo.h>
#include <Arduino.h>
#include <NewPing.h>

Servo servo1;
const int trig = 9;
const int echo = 10;

NewPing sonar(trig, echo, 40);
unsigned sm = sonar.ping_cm();

void setup() {
  servo1.attach(5);
  pinMode(trig, OUTPUT);

}

void loop() {
  sm = sonar.ping_cm();
  if (sm > 0 && sm <= 5) {
    delay(2000);
    sm = sonar.ping_cm();
    if (sm > 0 && sm <= 5) {

      servo1.write(17 );
      delay(1500);
      servo1.write(180);
      delay(1500);
      servo1.write(17);
 }
    else {
      servo1.write(15);
      delay(1500);
    }
  }
  else {
    servo1.write(15);
    delay(1500);
  }
}
